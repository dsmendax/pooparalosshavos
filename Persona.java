import java.util.Vector;
public class Persona{
    private int edad;
    private String nombre;
    public Persona(int edad, String nombre){
        this.edad = edad;
        this.nombre = nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setEdad(int edad){
        this.edad = edad;
    }

    public int getEdad(){
        return edad;
    }

    public String getNombre(){
        return nombre;
    }
}